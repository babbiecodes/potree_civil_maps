var http = require('http');
var fs = require('fs');

const PORT=8080; 

fs.readFile('./examples/test.html', function (err, html) {

	if (err) 
	{
		response.writeHead(404);
		throw err; 

	}    

    http.createServer(function(request, response) {  
        
        response.write(html);  
        response.end();  
    }).listen(PORT);
});

