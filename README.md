
# README

Hello there! This was my attempt to complete the first challenge from Civil Maps! I took the guidelines seriously and timeboxed
the assignment to about 3 hours. I was able to successfully link to the custom cloud.js file on AWS by implementing CORS on the
viewer.html page. The attempt to add the PointerLockControls from three.js proved to take some time, and was not able to complete
that part. 

## Build

Make sure you have [node.js](http://nodejs.org/) installed

Install all dependencies, as specified in package.json, 
then, install the gulp build tool:

    cd <potree_directory>
    npm install --save
    npm install -g gulp
    npm install -g rollup

Use the ```gulp watch``` command to 

* start a web server at localhost:1234. Go to http://localhost:1234/examples/ to test the examples.

## Files Authored

viewer.html